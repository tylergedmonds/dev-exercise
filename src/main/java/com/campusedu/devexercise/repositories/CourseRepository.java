package com.campusedu.devexercise.repositories;

import com.campusedu.devexercise.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    List<Course> findByCourseCodeNumber(int ccn);                                   //find only by ccn
    List<Course> findByCourseCodePrefix(String ccp);                                //find only by ccp
    List<Course> findByCourseCodePrefixAndCourseCodeNumber(String ccp, int ccn);    //find courses that contain both the ccn and ccp
    List<Course> findAll();                                                         //find all courses
}
