package com.campusedu.devexercise.controller;

import com.campusedu.devexercise.model.Course;
import com.campusedu.devexercise.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController //auto serializes response to JSON
public class CourseController {

//    API
//    ccp = Course Code Prefix
//    ccn = Course Code Number

    @Autowired
    private CourseRepository courseRepository;

    @GetMapping(value = "/courseInfo", produces = "application/json; charset=UTF-8")
//    @ResponseBody - this would be used to tell the controller the object returned is auto serialized into JSON and passed to HTTPResponse Object.
//    @RestController annotated controllers don't need to include this as @RestController does it automatically.
    public List<Course> courseInfo(
            @RequestParam(name = "ccp", required = false, defaultValue = "-1") String ccp,
            @RequestParam(name = "ccn", required = false, defaultValue = "-1") int ccn) {

        if (ccp.equals("-1") && ccn == -1) {
            return courseRepository.findAll();
        } else if (ccp.equals("-1")) {
            return courseRepository.findByCourseCodeNumber(ccn);
        } else if (ccn == -1) {
            return courseRepository.findByCourseCodePrefix(ccp.toUpperCase());
        } else {
            return courseRepository.findByCourseCodePrefixAndCourseCodeNumber(ccp.toUpperCase(), ccn);
        }
    }


}
