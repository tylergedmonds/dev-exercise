package com.campusedu.devexercise.model;


import javax.persistence.*;

@Entity
@Table(name = "course_data")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String advising_requisite_code;
    private String core_literature_requirement;
    private String course_catalog_text;
    private String course_code;
    private int courseCodeNumber;
    private String courseCodePrefix;
    private String course_code_sanitized;
    private int course_fees;
    private String course_title;
    private int default_credit_hours;
    private String division_code;
    private String fee_type;
    private String fixed_variable_credit;
    private int max_credit_hours;
    private int min_credit_hourse;

    protected Course() {}

    public Course(long id, long id1, String advising_requisite_code, String core_literature_requirement,
                  String course_catalog_text, String course_code, int courseCodeNumber,
                  String courseCodePrefix, String course_code_sanitized, int course_fees,
                  String course_title, int default_credit_hours, String division_code, String fee_type,
                  String fixed_variable_credit, int max_credit_hours, int min_credit_hourse) {
        this.id = id;
        this.advising_requisite_code = advising_requisite_code;
        this.core_literature_requirement = core_literature_requirement;
        this.course_catalog_text = course_catalog_text;
        this.course_code = course_code;
        this.courseCodeNumber = courseCodeNumber;
        this.courseCodePrefix = courseCodePrefix;
        this.course_code_sanitized = course_code_sanitized;
        this.course_fees = course_fees;
        this.course_title = course_title;
        this.default_credit_hours = default_credit_hours;
        this.division_code = division_code;
        this.fee_type = fee_type;
        this.fixed_variable_credit = fixed_variable_credit;
        this.max_credit_hours = max_credit_hours;
        this.min_credit_hourse = min_credit_hourse;
    }

    public Long getId() {
        return id;
    }

    public String getAdvising_requisite_code() {
        return advising_requisite_code;
    }

    public String getCore_literature_requirement() {
        return core_literature_requirement;
    }

    public String getCourse_catalog_text() {
        return course_catalog_text;
    }

    public String getCourse_code() {
        return course_code;
    }

    public int getCourse_code_number() {
        return courseCodeNumber;
    }

    public String getCourse_code_prefix() {
        return courseCodePrefix;
    }

    public String getCourse_code_sanitized() {
        return course_code_sanitized;
    }

    public int getCourse_fees() {
        return course_fees;
    }

    public String getCourse_title() {
        return course_title;
    }

    public int getDefault_credit_hours() {
        return default_credit_hours;
    }

    public String getDivision_code() {
        return division_code;
    }

    public String getFee_type() {
        return fee_type;
    }

    public String getFixed_variable_credit() {
        return fixed_variable_credit;
    }

    public int getMax_credit_hours() {
        return max_credit_hours;
    }

    public int getMin_credit_hourse() {
        return min_credit_hourse;
    }


}
